const R = require('ramda');
const percentile = require('./percentile');
const cities = require('./cities.json');
const table = require('text-table');

const KtoC = k => k - 273.15;
const KtoF = k => (k * 9) / 5 - 459.67;

// manual Currying
// const updateTemperature = conversionFn => city => {
// 	const temp = Math.round(conversionFn(city.temp));
// 	return R.mergeRight(city, { temp });
// };

// ramda currying
const updateTemperature = R.curry((conversionFn, city) => {
	const temp = Math.round(conversionFn(city.temp));
	return R.mergeRight(city, { temp });
});

const updatedCities = R.map(updateTemperature(KtoF), cities);
// console.log(R.slice(0, 2, updatedCities));

const totalCostReducer = (acc, city) => {
	const { cost = 0 } = city;
	return acc + cost;
};

const totalCost = R.reduce(totalCostReducer, 0, updatedCities);
const { length } = cities;
// console.log(totalCost / length);

const groupByPropReducer = (acc, city) => {
	const { cost = [], internetSpeed = [] } = acc;
	return R.mergeRight(acc, {
		cost: R.append(city.cost, cost),
		internetSpeed: R.append(city.internetSpeed, internetSpeed),
	});
};
const groupByProp = R.reduce(groupByPropReducer, {}, updatedCities);
// console.log(groupByProp);

const calcScore = city => {
	// defaulting and renaming a value in a destructuring syntax
	const { cost: newCost = 0, internetSpeed: newInternetSpeed = 0 } = city;
	const { cost, internetSpeed } = groupByProp;
	const costPercentile = percentile(cost, newCost);
	const internetSpeedPercentile = percentile(internetSpeed, newInternetSpeed);
	const score = 100 * (1.0 - costPercentile) + 20 * internetSpeedPercentile;
	return R.merge(city, { score });
};

const scoredCities = R.map(calcScore, updatedCities);

//predicate function: must resolve to either true or false.
const filterByWeather = ({ temp = 0, humidity = 0 }) => temp > 68 && temp < 85 && humidity > 30 && humidity < 70;

const filteredCities = R.filter(filterByWeather, scoredCities);

// console.log(R.length(filteredCities));

const sortedCities = R.sortWith([R.descend(R.prop('score'))], filteredCities);

// console.log(sortedCities);

const top10 = R.take(10, sortedCities);

// console.log(top10);
const interestingProps = ['Name', 'Country', 'Score', 'Cost', 'Temp', 'Internet Speed'];

const cityToArray = ({ name, country, score, cost, temp, internetSpeed }) => [
	name,
	country,
	score,
	cost,
	temp,
	internetSpeed,
];

const top11 = R.pipe(
	R.map(updateTemperature(KtoF)),
	R.filter(filterByWeather),
	R.map(calcScore),
	R.sortWith([R.descend(R.prop('score'))]),
	R.take(10),
	R.map(cityToArray),
	R.prepend(interestingProps),
	table,
)(cities);

console.log(top11);
