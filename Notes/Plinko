In the plinko game a ball is dropped and after a series of bumps it arrives to a bucket.
The goal here would be:

Given some data about where a ball is dropped from, can we predict what bucket it will end up in?

We should follow the previously described the problem solving process.

1. Identifying data that is relevant to the problem.

The first piece of data to record would be the position from where the ball is dropped,
The second piece of data is which bucket the ball falls into.
The third piece of data is the bounciness ratio of the ball.
The fourth piece is the ball size.

All these elements must be considered to make the prediction. However, we need to identify the
features (independent variables) and the labels (dependent variables)

Features: (Changing one of these...)

1. Drop position
2. Ball bounciness
3. Ball size

Labels: (... Will affect this)

1. Bucket a ball lands in

2. Assembling the set of data to the problem you're trying to solve.

In Javascript we have two approaches to store the data that we will use to model this problem
One is an array of objects with kvps and the second is an array of arrays where each element is
a collection of specifically ordered items to process the data. In the second approach we need
to have very present which index has which value.

In this example
1. 
[
  { dropPosition: 300, bounciness: 0.4, size: 16, bucket: 8 },
  .
  .
  .
]

2.
[
  [300, 0.4, 16, 8]
]

After setting the data we will analyze we need to:

3. Decide on the type of output we are predicting.

As we need to determine in which bucket the ball will fall into, we are having a classification problem
in which we have 10 different discrete set of values (finite set of buckets the ball could fall, there is
no in between buckets, there's no 1.5 bucket). Classification doesn't have to be a binary choice.

4. Pick an algorithm:

In this case Stephen picked a certain classification algorithm because is one of the few algorithms that 
intuitively make sense.

K-Nearest Neighbor: knn ('Birds of a feather flock together').

If we have a bunch of birds sitting together and we know those are cardinals, if another bird arrives and
look similar, it's very likely that that bird is a cardinal as well.
More general, if we have a group of things sitting together, they're likely to be the same thing.

Thought experiment:
What would happen if we dropped a ball ten time from around the same spot?.

KNN looks in the results of those inputs and make an approximation of recognized patterns. The flow would be:

1. Drop a ball a bunch of times all around the board, record which bucket it goes into
2. For each observation, subtract drop point from 300px and take the absolute value.
3. Sort the results from least to greatest
4. Look at the k top records, what is the most common bucket? (k is a variable, number of top records to considerate)
5. Whichever bucket came up most frequently is the one ours will probably go into.

In this implementation of the algorithm we will ignore the other features, ie. this will be a one variable
knn algorithm.

After testing the knn implementation we saw that we have a bad prediction.
To tackle this, we may need to:

1. Adjust the parameters of the analysis.
2. Add more features to explain the analysis
3. Change prediction point.
4. Accept that maybe there is not a good correlation.

However, this is pointless if we don't have a good way to compare accuracy with different settings.

