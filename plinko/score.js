const outputs = [];
const onScoreUpdate = (dropPosition, bounciness, size, bucketLabel) =>
	outputs.push([dropPosition, bounciness, size, bucketLabel]);

// K is the top n records and take the most common of those records.
// const k = 3;
// const predictionPoint = 300; // prediction point will be dynamic when extracting accuracy

// Fisher-Yates shuffle
const shuffleArray = array => {
	// added to force immutability
	const a = [...array];

	// impure code, how do you make a random shuffle pure? is it even possible?
	for (let i = a.length - 1; i > 0; i--) {
		const j = Math.floor(Math.random() * (i + 1));
		[a[i], a[j]] = [a[j], a[i]];
	}
	return a;
};

// distance with multiple variable is still finding a straight line between all points charted.
// So when they're charted we can extract the distance using the pythagorean theorem c^2 = a^2 + b^2
// As a general rule: To add more variables in consideration for the knn algorithm we must modify
// the distance formula.
// For 3d d^2 = a^2 + b^2 + c^2
// For 4 we add a '+ e^2', etc.

// One variable (reference point)
// const distance = (point, predictionPoint) => Math.abs(R.subtract(point, predictionPoint));

const distance = (pointA, pointB) =>
	// here point a and point b can be not numbers, but coordinates.
	R.pipe(
		R.map(([a, b]) => (a - b) ** 2),
		R.sum,
		n => n ** 0.5,
	)(R.zip(pointA, pointB));

const toInt = rows => rows.map(row => row.map(number => parseInt(number)));

// to debug the pipeline
// logs takes a value, performs a side effect with it, and then returns a value
const logs = (...args) => data => {
	console.log.apply(null, args.concat([data]));
	return data;
};

// Original implementation
// const knn = (dataSet, point, k) =>
// 	// R.tap run a transducer in the pipeline and returns the same input
// 	R.pipe(
// 		// For each observation, subtract drop point from 300, take absolute value and the bucket
// 		R.map(row => [distance(row[0], point), row[3]]),
// 		// Sort results from least to greatest
// 		R.sortBy(R.prop(0)),
// 		// Look at the k top records, What is the most common bucket?
// 		R.slice(0, k),
// 		// object which makes an histogram of the most common values
// 		R.countBy(R.prop(1)),
// 		// turn the previous object to an array of the kvps
// 		R.toPairs,
// 		toInt,
// 		R.sortBy(R.prop(1)),
// 		// sort from least to greatest the bucket frequency.
// 		// grab the last element, which will be the most common bucket.
// 		R.last,
// 		// grab the most common bucket for a single point
// 		R.head,
// 	)(dataSet);

// Universal KNN (where the point only has the features, not labels)
const knn = (dataSet, point, k) =>
	R.pipe(
		R.map(row => [distance(R.init(row), point), R.last(row)]),
		R.sortBy(R.prop(0)),
		R.slice(0, k),
		R.countBy(R.prop(1)),
		R.toPairs,
		toInt,
		R.sortBy(R.prop(1)),
		R.last,
		R.head,
	)(dataSet);

const splitDataSet = (data, testCount) => {
	const shuffled = shuffleArray(data);
	return [R.slice(0, testCount, shuffled), R.slice(testCount, Infinity, shuffled)];
};

const normalize = points => {
	const min = R.reduce(R.min, Infinity, points);
	const max = R.reduce(R.max, -Infinity, points);
	return R.map(point => (point - min) / (max - min) || 0, points);
};

// featureCount is because we only want to normalize the features, not the labels.
// here my brain is fucked and cant figure a way to make this work dynamically
const dataNormalization = (points, featureCount) => {
	const labels = R.map(R.slice(featureCount, Infinity), points);
	const featuresOnly = R.map(R.slice(0, featureCount), points);
	const firstLine = normalize(R.map(R.prop(0), featuresOnly));
	const secondLine = normalize(R.map(R.prop(1), featuresOnly));
	const thirdLine = normalize(R.map(R.prop(2), featuresOnly));
	const zipped = R.zip(R.zip(firstLine, secondLine), thirdLine);
	const allZipped = R.map(R.flatten, zipped);
	const withLabels = R.zip(allZipped, labels);
	return R.map(R.flatten, withLabels);
};

// Write code here to analyze stuff
function runAnalysis() {
	/** The next lines do something interesting: To extract an accuracy percentage, the first
	 * line extracts a random test set of a defined number, then it runs the knn algorithm in the
	 * remaining set called the trainingSet, knn(trainingSet, prediction), which will receive
	 * a prediction. Then it proceeds to build a new array which will hold:
	 * [knn prediction for a point, the actual value of the point]
	 * This will get an accuracy percentage
	 */
	// shuffle and extract a testSet
	const dataSize = 50;
	const [testSet, trainingSet] = splitDataSet(dataNormalization(outputs, 3), dataSize);
	// run the algorithm on the test set and compare it to the actual value to extract tuples
	// const buckets = R.map(testRow => {
	// 	const prediction = knn(trainingSet, testRow[0]);
	// 	const [, , , actual] = testRow;
	// 	return [prediction, actual, prediction === actual];
	// }, testSet);

	// get the accuracy percentage
	// const counting = R.countBy(R.prop(2))(buckets);
	// console.log(counting);
	// Refactor with function composition
	const accuracyPercentage = k =>
		R.pipe(
			R.filter(testPoint => knn(trainingSet, R.init(testPoint), k) === testPoint[3]),
			R.length,
			R.divide(R.__, dataSize),
			R.multiply(100),
		)(testSet);

	// console.log(`Accuracy: ${accuracyPercentage(3)}%`);
	// We need to vary k to find its most accurate value, to vary we create a ranged array and apply the
	// algorithm with that k tuning.
	const accuracies = R.pipe(
		R.addIndex(R.map)((k, index) => ({ accuracy: accuracyPercentage(k), index })),
		R.map(({ accuracy, index }) => `Accuracy for k=${index + 1} was ${accuracy}%`),
	)(R.range(0, 50));
	console.log(accuracies);
	// console.log(buckets);
	/** There was an issue with the prediction because the magnitudes of the variables are vastly different
	 * the bounciness factor varies between 0.5 and 0.55, contributing almost zero to the drop position
	 * factor, which varies from 10 to 700.
	 *
	 * There are a couple of ways of dealing with this kind of issues. The first one is normalization.
	 * Normalization means that we try to scale proportionately in which both labels vary from 0 to 1.
	 *
	 * The second one is standardization, here, instead of bounding a limit from 0 to 1, we try to base all of
	 * our numbers around zero, and calculate a normalized distribution between -1 and 1.
	 *
	 * For normalization we need to process each dataSet one by one, and each value can be extracted with a
	 * simple math equation.
	 *
	 * normalized data set = (featureValue - minOfFeatureValues) / maxOfFeatureValues - minOfFeatureValues
	 *
	 * for this to work, every single feature of the data set must be normalized
	 */
	// console.log(dataNormalization(outputs, 3));
	// console.log(outputs);

	// const absoluteWithBuckets = console.log('Ball will probably drop at', absoluteWithBuckets);
}
